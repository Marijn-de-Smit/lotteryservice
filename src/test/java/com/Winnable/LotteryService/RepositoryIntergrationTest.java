package com.Winnable.LotteryService;

import enums.Category;
import domain.Lottery;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import repository.ILotteryRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
public class RepositoryIntergrationTest {

    @MockBean
    private ILotteryRepository lotteryRepository;

    Optional<Lottery> lotteryOptional;
    
    List<Lottery> lotteries = new ArrayList<Lottery>();

    @Before
    public void SetUp() {
        Mockito.when(lotteryRepository.save(any(Lottery.class))).thenReturn(new Lottery(12, "updated", Category.CASH));
        Mockito.when(lotteryRepository.findAll()).thenReturn(lotteries);
        Mockito.when(lotteryRepository.findById(12)).thenReturn(lotteryOptional);
    }

    @Test
    public void createFuel() {
        // when
        Lottery item = new Lottery(12, "updated", Category.CASH);
        Lottery inserted = lotteryRepository.save(item);
        lotteries.add(item);

        // then
        assertThat(inserted.getId()).isEqualTo(item.getId());
    }

    @Test
    public void getAllFuel() {
        // when
        List<Lottery> found = lotteryRepository.findAll();

        // then
        assertThat(found.size()).isEqualTo(1);
    }

    @Test
    public void getFuelById() {
        // when
        Optional<Lottery> found = lotteryRepository.findById(12);

        // then
        assertThat(found.get().getId()).isEqualTo(12);
    }

    @Test
    public void updateFuel() {
        // when
        Optional<Lottery> beforeUpdate = lotteryRepository.findById(12);
        Lottery update = beforeUpdate.get();
        update.setName("updated");
        lotteryRepository.save(update);
        Optional<Lottery> afterUpdate = lotteryRepository.findById(12);

        // then
        assertThat(beforeUpdate.get().getName()).isNotEqualTo(afterUpdate.get().getName());
    }

    @Test
    public void DeleteFuel() {
        // when
        List<Lottery> beforeDelete = lotteryRepository.findAll();
        Lottery item = new Lottery(12, "updated", Category.CASH);
        lotteryRepository.delete(item);
        List<Lottery> afterDelete = lotteryRepository.findAll();

        // then
        assertThat(beforeDelete.size()).isNotEqualTo(afterDelete.size());
    }

}
