package rest;

import com.google.gson.Gson;
import domain.Lottery;
import dto.createDto.CreateLotteryDTO;
import dto.readDto.ReadLotteryDTO;
import dto.updateDto.UpdateLotteryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.ILotteryService;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@RestController
public class LotteryController implements ILotteryController {

    @Autowired
    private ILotteryService lotteryService;
    private Gson gson = new Gson();

    @Override
    public Response createLottery(String createLotteryDTO) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        CreateLotteryDTO lotteryDTO = gson.fromJson(createLotteryDTO, CreateLotteryDTO.class);
        Lottery lottery = new Lottery(lotteryDTO);

        Lottery createdLottery = lotteryService.createLottery(lottery);

        if (createdLottery != null) {
            ReadLotteryDTO readLotteryDTO = new ReadLotteryDTO(createdLottery);
            response.status(Response.Status.OK).entity(readLotteryDTO);
        }
        return response.build();
    }

    @Override
    public Response updateLottery(String updateLotteryDTO) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        UpdateLotteryDTO lotteryDTO = gson.fromJson(updateLotteryDTO, UpdateLotteryDTO.class);
        Lottery lottery = new Lottery(lotteryDTO);

        Lottery updatedLottery = lotteryService.updateLottery(lottery);

        if (updatedLottery != null) {
            ReadLotteryDTO readLotteryDTO = new ReadLotteryDTO(updatedLottery);
            response.status(Response.Status.OK).entity(readLotteryDTO);
        }

        return response.build();
    }

    @Override
    public Response deleteLottery(int id) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        Lottery lottery = new Lottery(id);

        boolean isRemoved = lotteryService.deleteLottery(lottery);
        response.status(Response.Status.OK).entity(isRemoved);

        return response.build();
    }

    @Override
    public Response getLottery(int id) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        Lottery lottery = lotteryService.getLottery(id);

        if (lottery != null) {
            ReadLotteryDTO readLotteryDTO = new ReadLotteryDTO(lottery);
            response.status(Response.Status.OK).entity(readLotteryDTO);
        }
        return response.build();
    }

    @Override
    public Response getAllLotteries() {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        List<Lottery> lotteryList = lotteryService.getAllLotteries();

        if (!lotteryList.isEmpty()) {
            List<ReadLotteryDTO> readLotteryDTOList = new ArrayList<>();
            for (Lottery lotteryItem : lotteryList) {
                ReadLotteryDTO readLotteryDTO = new ReadLotteryDTO(lotteryItem);
                readLotteryDTOList.add(readLotteryDTO);
            }
            response.status(Response.Status.OK).entity(readLotteryDTOList);
        }
        return response.build();
    }
}
