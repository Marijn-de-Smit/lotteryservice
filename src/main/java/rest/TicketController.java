package rest;

import com.google.gson.Gson;
import domain.Ticket;
import dto.createDto.CreateTicketDTO;
import dto.readDto.ReadTicketDTO;
import dto.updateDto.UpdateTicketDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import service.ITicketService;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@RestController
public class TicketController implements ITicketController{

    @Autowired
    private ITicketService ticketService;
    private Gson gson = new Gson();

    @Override
    public Response createTicket(String createTicketDTO) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        CreateTicketDTO ticketDTO = gson.fromJson(createTicketDTO, CreateTicketDTO.class);
        Ticket ticket = new Ticket(ticketDTO);

        Ticket createdTicket = ticketService.createTicket(ticket);

        if (createdTicket != null) {
            ReadTicketDTO readTicketDTO = new ReadTicketDTO(createdTicket);
            response.status(Response.Status.OK).entity(readTicketDTO);
        }
        return response.build();
    }

    @Override
    public Response updateTicket(String updateTicketDTO) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        UpdateTicketDTO ticketDTO = gson.fromJson(updateTicketDTO, UpdateTicketDTO.class);
        Ticket ticket = new Ticket(ticketDTO);

        Ticket updatedTicket = ticketService.updateTicket(ticket);

        if (updatedTicket != null) {
            ReadTicketDTO readTicketDTO = new ReadTicketDTO(updatedTicket);
            response.status(Response.Status.OK).entity(readTicketDTO);
        }

        return response.build();
    }

    @Override
    public Response deleteTicket(int id) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        Ticket ticket = new Ticket(id);

        boolean isRemoved = ticketService.deleteTicket(ticket);
        response.status(Response.Status.OK).entity(isRemoved);

        return response.build();
    }

    @Override
    public Response getTicket(int id) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        Ticket ticket = ticketService.getTicket(id);

        if (ticket != null) {
            ReadTicketDTO readTicketDTO = new ReadTicketDTO(ticket);
            response.status(Response.Status.OK).entity(readTicketDTO);
        }
        return response.build();
    }

    @Override
    public Response getAllTickets() {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        List<Ticket> ticketList = ticketService.getAllTickets();

        if (!ticketList.isEmpty()) {
            List<ReadTicketDTO> readTicketDTOList = new ArrayList<>();
            for (Ticket ticketItem : ticketList) {
                ReadTicketDTO readTicketDTO = new ReadTicketDTO(ticketItem);
                readTicketDTOList.add(readTicketDTO);
            }
            response.status(Response.Status.OK).entity(readTicketDTOList);
        }
        return response.build();
    }
}
