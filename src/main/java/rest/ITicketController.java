package rest;

import org.springframework.web.bind.annotation.*;
import javax.ws.rs.core.Response;

@RequestMapping("lottery/ticket")
public interface ITicketController {

    @PostMapping("/create")
    Response createTicket(@RequestBody String createTicketDTO);

    @PutMapping("/update")
    Response updateTicket(@RequestBody String updateTicketDTO);

    @DeleteMapping("/delete/{id}")
    Response deleteTicket(@PathVariable int id);

    @GetMapping("/get/{id}")
    Response getTicket(@RequestParam int id);

    @GetMapping("/all")
    Response getAllTickets();
}
