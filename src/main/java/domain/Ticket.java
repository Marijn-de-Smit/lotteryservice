package domain;

import dto.createDto.CreateTicketDTO;
import dto.updateDto.UpdateTicketDTO;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Ticket {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private int id;

    private int number;
    private int price;
    private int lotteryID;
    private int userID;

    public Ticket() {
    }

    public Ticket(int id) {
        this.id = id;
    }

    public Ticket(int number, int price, int lotteryID) {
        this.number = number;
        this.price = price;
        this.lotteryID = lotteryID;
    }

    public Ticket(CreateTicketDTO createTicketDTO) {
        this.number = createTicketDTO.getNumber();
        this.price = createTicketDTO.getPrice();
        this.lotteryID = createTicketDTO.getLotteryID();
    }

    public Ticket(UpdateTicketDTO updateTicketDTO) {
        this.id = updateTicketDTO.getId();
        this.price = updateTicketDTO.getPrice();
        this.lotteryID = updateTicketDTO.getLotteryID();
        this.userID = updateTicketDTO.getUserID();
    }

    public int getId() {
        return number;
    }

    public int getNumber() {
        return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }

    public int getPrice() {
        return price;
    }
    public void setPrice(int price) {
        this.price = price;
    }

    public int getLotteryID() {
        return lotteryID;
    }
    public void setLotteryID(int lotteryID) {
        this.lotteryID = lotteryID;
    }

    public int getUserID() {
        return userID;
    }
    public void setUserID(int userID) {
        this.userID = userID;
    }
}
