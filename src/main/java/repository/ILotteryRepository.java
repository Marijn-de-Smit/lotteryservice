package repository;

import domain.Lottery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ILotteryRepository extends JpaRepository<Lottery, Integer> {

    Lottery getById(int id);
}
