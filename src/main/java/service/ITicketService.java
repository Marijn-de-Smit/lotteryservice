package service;

import domain.Ticket;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ITicketService {

    Ticket createTicket(Ticket ticket);
    Ticket updateTicket(Ticket ticket);
    boolean deleteTicket(Ticket ticket);
    Ticket getTicket(int id);
    List<Ticket> getAllTickets();
}
