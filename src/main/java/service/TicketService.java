package service;

import domain.Ticket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.ITicketRepository;

import java.util.List;

@Service
public class TicketService implements ITicketService {

    @Autowired
    private ITicketRepository ticketRepository;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Ticket createTicket(Ticket ticket) {
        Ticket createdTicket = null;
        if (isValid(ticket)) {
            createdTicket = ticketRepository.save(ticket);
        }
        return createdTicket;
    }

    @Override
    public Ticket updateTicket(Ticket ticket) {
        Ticket updatedTicket = null;
        if (isValid(ticket)) {
            updatedTicket = ticketRepository.save(ticket);
        }
        return updatedTicket;
    }

    @Override
    public boolean deleteTicket(Ticket ticket) {
        boolean isRemoved = false;
        if (isValid(ticket)) {
            try {
                ticketRepository.delete(ticket);
                isRemoved = true;
            } catch (Exception ex) {
                logger.error(ex.getMessage());
            }
        }
        return isRemoved;
    }

    @Override
    public Ticket getTicket(int id) {
        Ticket ticket = null;
        if (id > 0) {
            ticket = ticketRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        }
        return ticket;
    }

    @Override
    public List<Ticket> getAllTickets() {
        return ticketRepository.findAll();
    }

    private boolean isValid(Ticket ticket) {
        return ticket.getPrice() != 0 ? true : false;
    }
}
