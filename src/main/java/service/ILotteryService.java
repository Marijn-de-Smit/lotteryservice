package service;

import domain.Lottery;
import dto.LotteryDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ILotteryService {

    Lottery createLottery(Lottery lottery);
    Lottery updateLottery(Lottery lottery);
    boolean deleteLottery(Lottery lottery);
    Lottery getLottery(int id);
    List<Lottery> getAllLotteries();
}
