package service;

import domain.Lottery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.ILotteryRepository;

import java.util.List;

@Service
public class LotteryService implements ILotteryService {

    @Autowired
    private ILotteryRepository lotteryRepository;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Lottery createLottery(Lottery lottery) {
        Lottery createdLottery = null;
        if (isValid(lottery)) {
            createdLottery = lotteryRepository.save(lottery);
        }
        return createdLottery;
    }

    @Override
    public Lottery updateLottery(Lottery lottery) {
        Lottery updatedLottery = null;
        if (isValid(lottery)) {
            updatedLottery = lotteryRepository.save(lottery);
        }
        return updatedLottery;
    }

    @Override
    public boolean deleteLottery(Lottery lottery) {
        boolean isRemoved = false;
        if (isValid(lottery)) {
            try {
                lotteryRepository.delete(lottery);
                isRemoved = true;
            } catch (Exception ex) {
                logger.error(ex.getMessage());
            }
        }
        return isRemoved;
    }

    @Override
    public Lottery getLottery(int id) {
        Lottery lottery = null;
        if (id > 0) {
            lottery = lotteryRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        }
        return lottery;
    }

    @Override
    public List<Lottery> getAllLotteries() {
        return lotteryRepository.findAll();
    }

    private boolean isValid(Lottery lottery) {
        return lottery.getCategory() != null && !lottery.getName().equals("") && lottery.getName() != null ? true : false;
    }
}