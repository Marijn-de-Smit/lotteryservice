package dto.readDto;

import domain.Lottery;
import enums.Category;

public class ReadLotteryDTO {

    private int id;
    private String name;
    private Category category;

    public ReadLotteryDTO(Lottery lottery) {
        this.id = lottery.getId();
        this.name = lottery.getName();
        this.category = lottery.getCategory();
    }

    public ReadLotteryDTO(int id, String name, Category category) {
        this.id = id;
        this.name = name;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Category getRoleType() {
        return category;
    }
}
