package dto.updateDto;

public class UpdateTicketDTO {

    private int id;
    private int number;
    private int price;
    private int lotteryID;
    private int userID;

    public UpdateTicketDTO(int id, int number, int price, int lotteryID, int userID) {
        this.id = id;
        this.number = number;
        this.price = price;
        this.lotteryID = lotteryID;
        this.userID = userID;
    }

    public int getId() {
        return id;
    }

    public int getNumber() {
        return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }

    public int getPrice() {
        return price;
    }
    public void setPrice(int price) {
        this.price = price;
    }

    public int getLotteryID() {
        return lotteryID;
    }
    public void setLotteryID(int lotteryID) {
        this.lotteryID = lotteryID;
    }

    public int getUserID() {
        return userID;
    }
    public void setUserID(int userID) {
        this.userID = userID;
    }
}
