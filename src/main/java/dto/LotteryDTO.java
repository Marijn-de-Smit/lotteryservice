package dto;

import enums.Category;

public class LotteryDTO {

    private int id;
    private String name;
    private Category category;

    public LotteryDTO() {
    }

    public LotteryDTO(String name, Category category) {
        this.name = name;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }
    public void setCategory(Category category) {
        this.category = category;
    }
}
