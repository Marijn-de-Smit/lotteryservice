package dto.deleteDto;

public class DeleteTicketDTO {

    private int id;

    public DeleteTicketDTO(int id) {
        this.id = id;
    }

    public int getId() { return id; }
}
