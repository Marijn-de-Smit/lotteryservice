package dto.createDto;

public class CreateTicketDTO {

    private int number;
    private int price;
    private int lotteryID;

    public CreateTicketDTO(int number, int price, int lotteryID) {
        this.number = number;
        this.price = price;
        this.lotteryID = lotteryID;
    }

    public int getNumber() {
        return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }

    public int getPrice() {
        return price;
    }
    public void setPrice(int price) {
        this.price = price;
    }

    public int getLotteryID() {
        return lotteryID;
    }
    public void setLotteryID(int lotteryID) {
        this.lotteryID = lotteryID;
    }
}
